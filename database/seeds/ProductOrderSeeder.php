<?php

use Illuminate\Database\Seeder;
use App\ProductOrder;

class ProductOrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\ProductOrder::class, 30)->create();
    }
}
