<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductsTableSeeder extends Seeder
{
    public function run()
    {
        for ($i = 1; $i <= 30; $i++) {
            Product::create([
                'name' => 'Станок бритвенный ' . $i,
                'slug' => 'razer-' . $i,
                'details' => [2, 3, 5][array_rand([3, 5, 7])] . ' лезвий, ' . [1, 2, 3][array_rand([2, 3, 4])] . ' сменных картриджа',
                'price' => rand(500, 6000),
                'count' => rand(1, 10),
                'description' => 'Lorem ' . $i . ' ipsum dolor sit amet!',
            ]);
        }

        $product = Product::find(1);
        $product->categories()->attach(2);
        for ($i = 1; $i <= 9; $i++) {
            Product::create([
                'name' => 'Флеш-карта ' . $i,
                'slug' => 'flashcard-' . $i,
                'details' => [16, 32, 64][array_rand([16, 32, 64])] . 'Gb' . ' USB ' . ['1.0', '2.0', '3.0'][array_rand(['1.0', '2.0', '3.0'])],
                'price' => rand(200, 10000),
                'count' => rand(1, 10),
                'description' => 'Lorem ' . $i . ' ipsum dolor sit amet!',
            ]);
        }

        Product::whereIn('id', [1, 12, 22, 31, 41, 43, 47, 51, 53, 61, 69, 73, 80])->update(['featured' => true]);
    }
}
